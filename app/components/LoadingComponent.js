import React from "react";
import { StyleSheet, Text, View, ActivityIndicator } from "react-native";
import { Overlay } from "react-native-elements/dist/overlay/Overlay";

const styles = StyleSheet.create({
    overlay: {
      height: 100,
      width: 200,
      backgroundColor: "rgba(40,100,140,0.45)",
      borderColor: "rgba(150,70,30,0.75)",
      borderWidth: 2,
      borderRadius: 10,
    },
    view: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    },
    text: {
      color: "#999999",
      textTransform: "uppercase",
      marginTop: 10,
    },
  });

export default function LoadingComponent(props) {
    const { visible, text } = props;

    return (
        <Overlay
          isVisible={visible?true:false}
          windowBackgroundColor="rgba(0, 0, 0, 0.5"
          overlayBackgroundColor="transparent"
          overlayStyle={styles.overlay}
        >
          <View style={styles.view}>
            <ActivityIndicator size="large" color="#999999" />
            {text?'pum':'pam' && <Text style={styles.text}>{text?'Cargando':'Espere por favor...'}</Text>}
          </View>
        </Overlay>
      );
}