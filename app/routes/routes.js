import 'react-native-gesture-handler';
import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Alert, Image, ImageBackground, 
    Dimensions, SafeAreaView, ScrollView, StyleSheet, 
    Pressable, Platform, StatusBar, ToastAndroid, BackHandler } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { DefaultTheme, NavigationContainer, getFocusedRouteNameFromRoute } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
//import theme from '../styles/theme.style';
//import Ionicons from 'react-native-vector-icons/Ionicons';
//import Feather from 'react-native-vector-icons/Feather';
import { scale, verticalScale, moderateScale } from '../styles/scaling';
//import { Utils, Validar, CheckConnectivity, Exponea } from '../helpers';
//import WSRestApi from '../services/wsRestApi';
import AsyncStorage from '@react-native-async-storage/async-storage';
//import changeNavigationBarColor from 'react-native-navigation-bar-color';
import OneSignal from 'react-native-onesignal';

//OneSignal Init Code
OneSignal.setLogLevel(6, 0);
OneSignal.setAppId("a51b9902-e163-455c-abcb-df06a8be4eba");
//END OneSignal Init Code

//Prompt for push on iOS
OneSignal.promptForPushNotificationsWithUserResponse(response => {
  console.log("Prompt response:", response);
});

//Method for handling notifications received while app in foreground
OneSignal.setNotificationWillShowInForegroundHandler(notificationReceivedEvent => {
  console.log("OneSignal: notification will show in foreground:", notificationReceivedEvent);
  let notification = notificationReceivedEvent.getNotification();
  console.log("notification: ", notification);
  const data = notification.additionalData
  console.log("additionalData: ", data);
  // Complete with null means don't show a notification.
  notificationReceivedEvent.complete(notification);
});

//Method for handling notifications opened
OneSignal.setNotificationOpenedHandler(notification => {
  console.log("OneSignal: notification opened:", notification);
});

const { width, height } = Dimensions.get('window');
const wd = 320;
const size = (width >= 768) ? scale(10) : scale(20);
const isIphoneX = DeviceInfo.hasNotch();

const StatusBarHeight = Platform.select({
    ios: isIphoneX == true ? 30 : 15,
    android: StatusBar.currentHeight,
    default: 0
})

const BarHeight = Platform.select({
    ios: isIphoneX == true ? 130 : 70,
    android: 60,
    default: 0
})

const BarHeightAuth = Platform.select({
    ios: isIphoneX == true ? 90 : 60,
    android: 60,
    default: 0
})

const TabBarHeight = Platform.select({
    ios: isIphoneX == true ? 80 : 50,
    android: 50,
    default: 0
})

const TabBarPaddingBottom = Platform.select({
    ios: isIphoneX == true ? 20 : 0,
    android: 0,
    default: 0
})

const navTheme = DefaultTheme;
navTheme.colors.background = '#D47156';

//----   HEADER   ----
//import LogoTitle from '../components/Header/LogoTitle.component';
//mport MenuTitle from '../components/Header/MenuTitle.component';
//import HeaderRight from '../components/Header/HeaderRight.component';
//import HeaderLeft from '../components/Header/HeaderLeft.component';
//import HeaderLeft4 from '../components/Header/HeaderLeft4.component';
//import HeaderLeft5 from '../components/Header/HeaderLeft5.component';
function HeaderLeft({ navigation, color }) {
    return (
        <View style={{ marginLeft: 20, marginTop: StatusBarHeight, }}>
            <TouchableOpacity onPress={() => {
                //console.log("<--- HeaderLeft ", navigation);
                //navigation.navigate("Login");
                navigation.goBack();
            }} >
                <Feather name="arrow-left" size={size} style={{ color: color }} />
            </TouchableOpacity>
        </View>
    );
}
function HeaderAuthLeft({ navigation, color }) {
    return (
        <View style={{ marginLeft: 20, marginTop: isIphoneX == true ? -10 : 0, }}>
            <TouchableOpacity onPress={() => {
                navigation.goBack();
            }} >
                <Feather name="arrow-left" size={size} style={{ color: color }} />
            </TouchableOpacity>
        </View>
    );
}

function HeaderRight({ navigation }) {
    let valor = 0;

    if(valor == 0){
        return (
            <View
                style={{
                    backgroundColor: 'transparent',
                    //marginTop: StatusBarHeight,
                }}>
                <TouchableOpacity onPress={ async () => {  
                    let resultado = Platform.OS === "ios" ? await _HandleValidate2(32) : await _HandleValidate(20);
                    if (resultado == true) {
                        navigation.navigate('Bienvenido');
                    }                            
                }} >
                   
                    
                </TouchableOpacity>
            </View>
        );  
    } /*<Text>hola</Text>*/


}

const styles = StyleSheet.create({
    supercont:{
        flex:1,
        //justifyContent:'center',
    },
    header:{ 
            height: 40,
            position: 'absolute',
            alignItems: 'center', //justifyContent:'center',
            left: 0,
            right: 0,
            top: 10,
            backgroundColor: 'transparent',
            zIndex: 10
    },
    container: {
        height: 90,
        width: width-20,
        paddingTop:30,
        paddingLeft: 10,
        paddingRight:10,
        paddingBottom:30,
        position: 'absolute',
        left: 10 - width / 2,
        right: 0,
        top: 80,
        backgroundColor: 'rgba(150, 94, 134, 0.6)',
        zIndex: 10,
        borderRadius:30,
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center', alignContent: 'center',
    },
    subcontsuper1:{
        flex:1, 
        flexDirection:'column',
    },
    subcontsuper2:{
        flex:0.5, 
        flexDirection:'column',
    },
    subcontsuperColumn:{
        flex:5, 
        flexDirection:'column',
        alignItems:'flex-end',
        paddingLeft:20,
        right:-10,
    },
    subcontsuperRow:{
        flex:1.5, 
        flexDirection:'row',
        alignContent:'flex-end'
    },
    subcontsuperRow2:{
        flex:4, 
        flexDirection:'row',alignContent:'flex-end'
    },
    subcont:{
      flex:1,
      paddingLeft:0,
      paddingBottom:0,
      paddingEnd:0,
      margin:5,
      flexDirection:'column',
    },
    subcont2:{
        flex:1, 
        visibility: 'gone', 
        flexDirection:'column',
      },
    touchMenu:{
        borderRadius: 1000,
    },
    imageMenu1:{
        width: Platform.OS === 'ios' ? '150%':'150%',//150 / 1.9 *1.7 : 150 / 2.1 *1.7,
        height: Platform.OS === 'ios' ? '115%':'115%',//45 / 1.9 *2.9 : 45 / 2.1 *2.9, 
        backgroundColor:'transparent', alignItems:'center',
        resizeMode:'center',
        borderRadius:20,
    },
    imageMenu2:{
        width: Platform.OS === 'ios' ? '115%': width>wd?'105%':'115%',//150 / 1.2 : 150 / 2.0,
        height: Platform.OS === 'ios' ? 55 / 1.2 : width>wd? 55 / 1.2:55 / 1.3,
        resizeMode:'center',
        backgroundColor:'transparent', alignItems:'center',
    },
    imageMenu:{
        width: Platform.OS === 'ios' ? '150%':'110%',//150 / 1.2 : 150 / 2.0,
        height: Platform.OS === 'ios' ? 45 / 1.2 : 45 / 1.4,
        resizeMode:'center',
        borderColor:'transparent', alignItems:'center',
    },
    textMenu:{
        color: '#fff',
        fontSize:14,
    },
  });

function MenuTitle({ navigation }){
    
    //handleClick = async () => {this.props.navigation.navigate("Probando")}this.handleClick
    const paramNameR = "Probando";
    const paramNameL = "Camera";
    const paramNameLo = "Inicio de sesión"; 
    const paramNameM = "Lista";
    const paramNameS = "Registro";
    const paramNameTim = "Mapa";

    return (
       <View style={styles.container} >
           <StatusBar animated={true} backgroundColor="#444444" />  
        <View style={styles.subcont}>
            <TouchableOpacity style={styles.touchMenu}
            onPress={()=>navigation.navigate({name:'Bienvenido',params:{new:false}})}> 
             <ImageBackground
                source={require('../assets/img/logo-icon.png')}
                style={styles.imageMenu2}
            ><Text style={styles.textMenu}>Inicio</Text></ImageBackground>        
        </TouchableOpacity>
        </View>       
        <View style={styles.subcont}>
        <TouchableOpacity  onPress={()=>navigation.navigate(paramNameL)}>
        <ImageBackground
                source={require('../assets/img/camera.png')}
                style={styles.imageMenu2}
            ><Text style={styles.textMenu}></Text></ImageBackground>
        </TouchableOpacity>
        </View>
        <View style={styles.subcont}>
        <TouchableOpacity  onPress={()=>navigation.navigate(paramNameM)}>
        <ImageBackground
                source={require('../assets/img/ws.png')}
                style={styles.imageMenu2}
                resizeMode='center'
            ><Text style={styles.textMenu}></Text></ImageBackground>
        </TouchableOpacity>
        </View> 
        <View style={styles.subcont}>
        <TouchableOpacity  onPress={()=>navigation.navigate({name:'Registro',params:{new:false}})}>
        <ImageBackground
                source={require('../assets/img/user.png')}
                style={styles.imageMenu2}
            ><Text style={styles.textMenu}></Text></ImageBackground>
        </TouchableOpacity>
        </View>
        <View style={styles.subcont}>
        <TouchableOpacity onPress={()=>navigation.navigate(paramNameTim)}>
        <ImageBackground
                source={require('../assets/img/geo.png')}
                style={styles.imageMenu2}
            ><Text style={styles.textMenu}></Text></ImageBackground>
        </TouchableOpacity>
        </View>
        <View style={styles.subcont}>
        <TouchableOpacity  onPress={()=>{
            //navigation.navigate(paramNameLo)
            Alert.alert ('Salida', '¿Está seguro de que desea salir del sistema?', 
            [{
                text: 'Cancelar',
              }, {
                text: 'Salir',
             onPress: () => {
                 BackHandler.exitApp();
         },
         }]);
        }}>
        <ImageBackground
                source={require('../assets/img/exit.png')}
                style={styles.imageMenu2}
            ><Text style={styles.textMenu}></Text></ImageBackground>
        </TouchableOpacity>
        </View></View>
    );
}

/*
<View style={styles.subcontsuper1}></View> 
<View style={styles.subcontsuper2}></View><View style={styles.subcontsuperColumn}><View style={styles.subcontsuperRow}>
</View><View style={styles.subcontsuperRow2}></View><View style={styles.subcontsuperRow}>
*/
function LogoTitle({ navigation }) {

    return (
        <View
            style={styles.header}>
   
            <TouchableOpacity onPress={
                async () => {
                    try {
                     let hayConexion = await CheckConnectivity.fnCheckConnectivity();
                        if (hayConexion) {
                                     navigation.navigate('Inicio - Formulario');
                        } else {
                                this.setState({ titleHint: 'Error de conexión' });
                                navigation.navigate('Inicio - Formulario');
                                //this.Hint.current.mostrarConParametros("No está conectado a internet, por favor encienda WiFi o 3G.");
                        }      
                    } catch (error) {
                        console.log(error);
                        //navigation.navigate('Inicio - Formulario');
                        this.setState({ titleHint: 'Error de conexión' });
                    }
                }
            }>
             <Image
                source={require('../assets/img/logo1.png')}
                style={{
                    width: Platform.OS === 'ios' ? 181 / 1.3 : 181 / 1.2,
                    height: Platform.OS === 'ios' ? 45 / 1.3 : 45 / 1.2,
                    borderColor:'#000'
                }}
            /> 
            </TouchableOpacity>
{/**/}
        </View>
    );
}


//---- AuthLoadingStack ----
//import SplashScreen from '../views/splashScreen';
//import initializingScreen from '../views/initializing.js';
//---- FIN AuthLoadingStack ----

//---- AuthStack ----
// import LoginScreen from '../views/Login/Login.view';
// import ActualizarApp from '../views/ActualizarApp/ActualizarApp.view';
// import ActivarHuellaScreen from '../views/ActivarHuella/ActivarHuella.view';
// import RecuperarScreen from '../views/RecuperarPassword/RecuperarPassword.view';
// import RecuperarScreen2 from '../views/RecuperarPassword/RecuperarPassword2.view';
// import IngresarPinScreen from '../views/RecuperarPassword/IngresarPin.view';
// import IngresarPasswordScreen from '../views/RecuperarPassword/IngresarPassword.view';
// import RegistroUsuarioScreen from '../views/RegistroUsuario/RegistroUsuario.view';
// import RegistroUsuario2Screen from '../views/RegistroUsuario/RegistroUsuario2.view';

//---- FIN AuthStack ----

//---- AppStack ----
import HomeScreen from '../views/Home.js';
import LoginScreen from '../views/LoginHome.js';
import OtraScreen from '../views/Home.js';
import ListaScreen from '../views/ListaWS';
import RegistroScreen from '../views/Registro.js';
import CameraScreen from '../views/CameraQR.js';
import MapasScreen from '../views/GeoSave.js';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function AppStack() {
    return (
        <Stack.Navigator
            initialRouteName="Homex"
            screenOptions={{
                headerTitleAlign: 'center',
                headerStyle: {                    
                    backgroundColor: '#D47156',
                    height: BarHeight 
                },
                tabBarVisible:false
            }}
            options={{
                gestureEnabled: false,
                tabBarVisible: false,
            }}>

             <Stack.Screen
            name="Inicio de sesión"
            component={LoginScreen}
            options={({ navigation, route }) => ({
                //headerLeft: props => <MenuTitle {...props} navigation={navigation} />,
                headerTitle: props => <View style={styles.supercont}><LogoTitle {...props} navigation={navigation} />
                {/*<MenuTitle {...props} navigation={navigation} />*/}</View>,
                //headerRight: props => <HeaderRight navigation={navigation} />
            })}
            />

            <Stack.Screen
            name="Probando"
            component={OtraScreen}
            options={({ navigation, route }) => ({
                //headerLeft: props => <MenuTitle {...props} navigation={navigation} />,
                headerTitle: props => <View style={styles.supercont}><LogoTitle {...props} navigation={navigation} />
                <MenuTitle {...props} navigation={navigation} /></View>,
                headerRight: props => <HeaderRight navigation={navigation} />
            })}
            />

            <Stack.Screen
                name="Bienvenido"
                component={HomeScreen}
                options={({ navigation, route }) => ({
                    //headerLeft: props => <MenuTitle {...props} navigation={navigation} />,
                    headerTitle: props => <View style={styles.supercont}><LogoTitle {...props} navigation={navigation} />
                    <MenuTitle {...props} navigation={navigation} /></View>,
                    //headerRight: props => <HeaderRight navigation={navigation} />
                })}
            />

           <Stack.Screen
                name="Lista"
                component={ListaScreen}
                options={({ navigation, route }) => ({
                    //headerLeft: props => <MenuTitle {...props} navigation={navigation} />,
                    headerTitle: props => <View style={styles.supercont}><LogoTitle {...props} navigation={navigation} />
                    <MenuTitle {...props} navigation={navigation} /></View>,
                    //headerRight: props => <HeaderRight navigation={navigation} />
                })}
            />

            <Stack.Screen
                name="Registro"
                component={RegistroScreen}
                options={({ navigation, route }) => ({
                    //headerLeft: props => <MenuTitle {...props} navigation={navigation} />,
                    headerTitle: props => <View style={styles.supercont}><LogoTitle {...props} navigation={navigation} />
                    </View>,
                    //headerRight: props => <HeaderRight navigation={navigation}<MenuTitle {...props} navigation={navigation} /> />
                })}
            /> 

           <Stack.Screen
                name="Camera"
                component={CameraScreen}
                options={({ navigation, route }) => ({
                    //headerLeft: props => <MenuTitle {...props} navigation={navigation} />,
                    headerTitle: props => <View style={styles.supercont}><LogoTitle {...props} navigation={navigation} />
                    </View>,
                    //headerRight: props => <HeaderRight navigation={navigation} />
                })}
            />

            <Stack.Screen
                name="Mapa"
                component={MapasScreen}
                options={({ navigation, route }) => ({
                    //headerLeft: props => <MenuTitle {...props} navigation={navigation} />,
                    headerTitle: props => <View style={styles.supercont}><LogoTitle {...props} navigation={navigation} />
                    </View>,
                    //headerRight: props => <HeaderRight navigation={navigation} />
                })}
            /> 
                                   
        </Stack.Navigator>
        //{/**/}
    );
}

function getTabBarVisible(route) {
    const routeName = getFocusedRouteNameFromRoute(route) ?? 'LoginHome';
    const hideOnScreens = [
        'Buzon', 'BuzonDetalle', 'Asistencia', 'AsistenciaDetalle', 'Config', 'Info', 'Password', 'SiniestroVehiculo', 
        'Prueba2', 'SeguimientoHome', 'DetalleReembolsoSaludHome' , 'Reembolso' ,'PagoEnLinea','VisorPDF',
        'Prueba', 'DenuncioSeguro', 'ConfirmacionDenunciaSeguro', 'DatosDelSiniestro' , 'DatosConstancia', 'DatosOtroConductor',
        'DenunciaSiniestro', 'Accidente2', 'DatosTerceroAcc', 'Accidente31', 'Accidente32', 'Accidente4', 'PantallaFinalAcc',
        'PantallaFinalError', 'PantallaFinalTermino', 'Robo', 'Robo21', 'Robo22', 'Robo3', 'DatosTercero', 'FinalActoMalicios',
        'ActoMalicioso', 'ActoMalicios21', 'ActoMalicios22', 'ActoMalicios3','Contactar2', 'Deposito', 'EditarDeposito' ,'ConfirmarCodigo' ,'ConfirmarDeposito' ,'ConfirmarCorreo','DatosActualizados',
    'Inicio de sesión','Bienvenido'];     
    if(hideOnScreens.indexOf(routeName) > -1) return false;
    return true;    
  }

function MainTabs() {
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
                    let anchoIcono = 60;
                    let altoIcono = 60;
                    switch (route.name) {
                        case 'Inicio':
                            if (focused) {
                                return <View style={{ flex: 1, width: '100%', visibility:false }}>
                                    <View style={{ top: 0, height: 2 , backgroundColor: '#006FB9', visibility:false }} ></View>
                                    <View style={{ flex: 1, backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center' }}>
                                        {/* <Image source={require('../assets/icon/c_home.png')} style={{ width: anchoIcono, height: altoIcono, opacity: focused ? 1 : 0.5 }} /> */}
                                    </View>
                                </View>
                            } else {
                                return <View style={{ flex: 1, width: '100%', }}>
                                    <View style={{ top: 0, height: 2, backgroundColor: 'transparent', }} ></View>
                                    <View style={{ flex: 1, backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center' }}>
                                        {/* <Image source={require('../assets/icon/c_home.png')} style={{ width: anchoIcono, height: altoIcono, opacity: focused ? 1 : 0.5 }} /> */}
                                    </View>
                                </View>
                            }
                            break;
                        
                    }
                },        
                    activeTintColor: '#595B5A',
                    inactiveTintColor: '#8F8F8F',
                    labelStyle: {                    
                        marginBottom: 5,
                        fontSize: 30
                    },                
                    defaultNavigationOptions: {
                        height: 0,
                        tabBarVisible: false
                      },
                      navigationOptions: {
                        height: 0,
                        lazy: true
                      }
            })}

        >
            <Tab.Screen name=" " 
                component={AppStack} 
                options={({ route }) => ({
                    headerShown:false,
                    gestureEnabled:false,
                    tabBarVisible: false /*getTabBarVisible(route)*/,
                    //{false},
                    defaultNavigationOptions: {
                        tabBarVisible: false
                      },
                      navigationOptions: {
                        lazy: true
                      } })
                }
                screenOptions={{
                    headerShown: false,
                    tabBarHideOnKeyboard: true,
                    showLabel: false,
                    tabBarStyle: {
                        elevation: 0,
                        backgroundColor: '#F1F1F1',
                        height: 70,
                        /*display: 'none',*/ //...styles.shadow
                    },
                    tabBarLabelStyle: {
                        display: 'none'
                    },
        
                }}
                defaultNavigationOptions= {{
                    tabBarVisible: false
                }}
                navigationOptions= {{
                    lazy: true
                }}             
            />
        
        </Tab.Navigator>
    );
}


function MainNavigator() {
    return (
        <Stack.Navigator>
            <Stack.Screen 
            name="Main" 
            component={AppStack}  
            options={{
                    tabBarLabel: ' ',
                    headerShown:false,
                    gestureEnabled:false,
                }}
                />
        </Stack.Navigator>
    );
}



class routes extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userToken: null,
        }
    }

    render() {
        return (
            <NavigationContainer
                theme={navTheme}
            >
                {/* {this.state.userToken == null ? <AuthNavigator/> : <MainNavigator/> } */}
                <Stack.Navigator>
                    {/* <Stack.Screen options={{ headerShown: false, gestureEnabled: false }} name="Initial" component={InitialNavigator} /> */}
                    {/* <Stack.Screen options={{ headerShown: false, gestureEnabled: false }} name="Auth" component={AuthNavigator} />  */}
                    <Stack.Screen options={{ headerShown: false, gestureEnabled: false }} name="App" component={MainNavigator} />
                </Stack.Navigator>

            </NavigationContainer>
        );
    }

}

export default routes;