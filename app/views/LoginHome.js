import React, {Component} from 'react';
import {useEffect} from 'react';
import { Text, View, Image, Button, StyleSheet, StatusBar, Modal, 
  ImageBackground, SafeAreaView, Dimensions, Keyboard, Alert } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { openDatabase } from 'react-native-sqlite-storage';
import LoadingComponent from '../components/LoadingComponent.js';

const { width, height } = Dimensions.get('window');
const wd = 320;
const hg = 634;
console.log(width,' ',height);

/*function _onPressFun(){
  //this.setModalVisibility(true);
  return (alert("Registro de informacion para guerreros."));
}*/

const styles = StyleSheet.create({
    principal:{

    },
    container: {
      flex: 1,
      //marginTop: StatusBar.currentHeight || 0,
      zIndex:10,
    },
    textform:{
      color: '#fff'
    },
    textInput:{
      borderColor: 'gray',
      borderWidth: 2
    },
    btn:{
      paddingHorizontal:8,
      paddingVertical: 1,
      borderRadius: 10,
      backgroundColor: "oldlace",
      alignSelf: "flex-start",
      marginHorizontal: "1%",
      minWidth: "48%",
      textAlign: "center"
    },
    item: {
      backgroundColor: '#f9c2ff',
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 16,
    },
    logo:{
       padding:2, 
       flex:2, //height<2323200?:true
       justifyContent:'center'
    },
    img:{
      height:height<hg? 100:200,
      width: height<hg? 100:200,
      backgroundColor: 'skyblue', 
      borderRadius: height<hg? 50:100
    },
    title: {
      fontSize: 32,
    },
  });

  
const CatApp = () => {
  return (
    <View style={{ flex: 1, alignItems: 'center',alignContent: 'space-around' , justifyContent: 'space-evenly' }}>
    </View>
  );
}


//export default CatApp;
export default class LoginHome extends Component {
  constructor(props) {
    super(props);

    //this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    //let db = openDatabase({ name: 'UserDatabase.db' });

    this.state = {
      postext:"",
      postext2:"",
      estado: 'oli',
      visibility: false,
      load: false,
      keyboardStatus: false,
    };

    //this.Hint = React.createRef();
  }
  
  componentDidMount() {

    let db = openDatabase({ name: 'UserDatabase.db' });

      db.transaction(function (txn) {
        txn.executeSql(
          "SELECT name FROM sqlite_master WHERE type='table' AND name='table_user'",
          [],
          function (tx, res) {
            console.log('item:', res.rows.length);
            if (res.rows.length == 0) {
              txn.executeSql('DROP TABLE IF EXISTS table_user', []);
              txn.executeSql(
                'CREATE TABLE IF NOT EXISTS table_user(user_id INTEGER PRIMARY KEY AUTOINCREMENT, user_name VARCHAR(20), user_pass VARCHAR(1000), user_contact INT(10), user_address VARCHAR(255))',
                []
              );
            }
          },
            function (tx, error) {
              console.log('error:', error);
              if (res.rows.length == 0) {
                txn.executeSql('DROP TABLE IF EXISTS table_user', []);
                txn.executeSql(
                  'CREATE TABLE IF NOT EXISTS table_user(user_id INTEGER PRIMARY KEY AUTOINCREMENT, user_name VARCHAR(20), user_contact INT(10), user_address VARCHAR(255))',
                  []
                );
             }
            },
            txn.executeSql(
              "SELECT name FROM sqlite_master WHERE type='table' AND name='table_position'",
              [],
              function (tx, res) {
                console.log('posi:', res.rows.length);
                if (res.rows.length == 0) {
                  txn.executeSql('DROP TABLE IF EXISTS table_position', []);
                  txn.executeSql(
                    'CREATE TABLE IF NOT EXISTS table_position(pos_id INTEGER PRIMARY KEY AUTOINCREMENT, pos_name VARCHAR(20), pos_lat VARCHAR(255), pos_lon VARCHAR(255))',
                    []
                  );
                }
              },
                function (tx, error) {
                  console.log('error2:', error);
                  if (res.rows.length == 0) {
                 }
           })
        );
      });
      /**/
      this.keyboardDidShowSubscription = Keyboard.addListener(
        'keyboardDidShow',
        () => {
          this.setState({ keyboardStatus: true });
          //console.log("keyb act")
        },
      );
      this.keyboardDidHideSubscription = Keyboard.addListener(
        'keyboardDidHide',
        () => {
          this.setState({ keyboardStatus: false });
          //console.log("keyb desact")
        },
      );
 }

 
componentWillUnmount = () => {
  //<LoadingComponent visible={false} />
  this.keyboardDidShowSubscription.remove();
  this.keyboardDidHideSubscription.remove();
};

  setModalVisibility(visible) {
    this.setState({
      visibility: visible,
    });
  }

  setPost1(value){
    this.setState({
      postext: value,
    })
  }

  setPost2(value){
    this.setState({
      postext2: value,
    })
  }

  _onPressFun(){
    //this.setModalVisibility(true);
    alert("Sistema hecho con React Native como ejemplo en version 3.1.");
  }

  _onPressFun2(){
    this.props.navigation.navigate({name:'Registro',params:{new:true}});
  }

  loadCom(a){ 
    return(<LoadingComponent visible={a}/>);
  }

  postMsg(){
      const t1 = this.state.postext;
      const t2 = this.state.postext2;
      //console.log(t1+t2);

      if (t1.trim().length > 0 && t2.trim().length > 0) {

      this.setState({
        load:true
      });
  
      setTimeout(() => {
        this.setState({
          load:false
        });
      }, 2000);

      setTimeout(() => {
        this.props.navigation.navigate({name:'Bienvenido',params:{text1: t1, text2: t2, new:true}});
      }, 600);
    } else{ 
      Alert.alert('Mensaje', 'Debe completar formulario sin campos vacios.');
      return;}
  }

  render(){
    return (
      <SafeAreaView style={styles.container}>
      <ImageBackground style={{ flex: 1,
            height: '100%',
            width: '100%'}}
            resizeMode="cover"
            source={require('./../assets/gif/fondo-ani.gif')}>
      <View navigation={this.props.navigation} 
      style={{ flex:1, alignItems: 'center',alignContent: 'space-around' , justifyContent: 'space-between' }}>
        <View style={(this.state.keyboardStatus && height<hg)?{flex:0.5}:
              (this.state.keyboardStatus && height>=hg)?{flex:1.0}:{flex:0.5}}></View>
        {<View style={{visible:this.state.load}}>{this.loadCom(this.state.load)}</View>
  } 
        <View style={[{justifyContent:'center'},
        (this.state.keyboardStatus && height<hg)?{paddingTop:30,flex:0.5}:
        (this.state.keyboardStatus && height>=hg)?{paddingTop:5,flex:0.3}:
        (!this.state.keyboardStatus && height<hg)?{paddingTop:30,flex:0.5}:
        {paddingTop:80,flex:0.5}]}>
        <Image
          source={require('../assets/img/1.png')}/*{uri: "https://reactnative.dev/docs/assets/p_cat1.png"}*/
          style={[styles.img,
            (this.state.keyboardStatus && height<hg)?{width:100/2,height:100/2}:
              (this.state.keyboardStatus && height>=hg)?{width:200/2,height:200/2}:{}
            ]}
        /></View>

        <View style={(this.state.keyboardStatus && height<hg)?{flex:0.5}:
              (this.state.keyboardStatus && height>=hg)?{flex:1.0}:{flex:1}} ></View>
        
        <View style={{ flex:1, width:'80%' }}>
        <Text style ={styles.textform}>Nombre-clave</Text>
        <TextInput 
          style ={styles.textInput}
          Name="campo1"
          value={this.state.postext}
          onChangeText={(v)=>this.setPost1(v)}
          placeholder="Ingresar nombre-clave"
          placeholderTextColor={"gray"}/>
        <Text style ={styles.textform}>Contraseña</Text>
        <TextInput 
          style ={styles.textInput}
          value={this.state.postext2}
          onChangeText={(v)=>this.setPost2(v)}
          Name="campo2"
          secureTextEntry={true} 
          placeholder="Ingresar contraseña"
          placeholderTextColor={"gray"}/>  
        </View>
        
        <View style={(this.state.keyboardStatus && height<hg)?{flex:2.5}:
              (this.state.keyboardStatus && height>=hg)?{flex:1.5}:{flex:1.5}} ></View>
        
        <View style={{ flex:1, flexDirection: 'row',alignContent: 'space-around', justifyContent: 'space-evenly'}}>
        <View ><Button
            style ={styles.btn}
            title="Iniciar sesion"
            onPress={() => this.postMsg()} /></View>
        <View style={{ minWidth:10}}></View>
        <View ><Button
            style ={styles.btn}
            title="Información"
            onPress={()=>this._onPressFun()}/></View>
        </View>
        <View style={{ flex:this.state.keyboardStatus?0:1, flexDirection: 'column',alignContent: 'space-around', justifyContent: 'space-evenly'}}
        >
        <Button
            style ={styles.btn}
            title="Registrarse"
            onPress={()=>this._onPressFun2()}/>
        </View>
        <View style={{ flex:this.state.keyboardStatus?0.1:0.5}}></View>
        <View style={{ flex:this.state.keyboardStatus?0.1:1}}>
        <Modal
          style={{ flex: 1, alignItems: 'center',alignContent: 'space-around' , justifyContent: 'space-evenly' }}
          animationType={'slide'}
          transparent={true}
          visible={this.state.visibility}>
          <View style={styles.modalContainer}>
            <View>
              <Text>Esta es la informacion de registro:</Text>
              <Text>-Mayor de 18 años.</Text>
              <Text>-No pertenecer a otros clanes.</Text>
              <Text>-En lo posible, ser un humano o gato!</Text>
              <Button
                color="#000"
                onPress={() => this.setModalVisibility(!this.state.visibility)}
                title="Cerrar información"
              />
            </View>
          </View>
        </Modal>
        </View>
      </View>
      </ImageBackground>
      </SafeAreaView>);
  }
}
