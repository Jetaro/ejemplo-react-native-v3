import * as React from 'react';
import { Component } from 'react';
import { View, Text, FlatList, Alert, Image, TouchableOpacity, 
  StyleSheet, SafeAreaView, ImageBackground } from 'react-native';
import { isLandscapeSync } from 'react-native-device-info';
import  LoadingComponent  from '../components/LoadingComponent.js';

class ListaWS extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      initialPosition: 'unknown',
      res:[],
      out:[],
      load: false
    };
   }
      

   renderSeparator = () => {  
    return (  
        <View  
            style={{  
                height: 1,  
                width: "100%",  
                backgroundColor: "#000",  
            }}  
        />  
      );  
  };  

loadCom(a){ 
  return(<LoadingComponent visible={a}/>);
}

  //handling onPress action  
getListViewItem = (item) => {  

    this.setState({
      load:true
    });

    setTimeout(() => {
      console.log("2 sec.")
    }, 2000);

    fetch(item.url)
          .then(response => response.json())
          .then(result => {this.setState({ out : result } );
        
          this.setState({
            load:false
          });

          if (this.state.out && this.state.out!=[]){
            Alert.alert(
               `Detalles del item ${item.name}`,
               `
               Id: ${JSON.stringify(this.state.out.id)}\n
               Peso: ${JSON.stringify(this.state.out.weight)} kgs \n
               Tipo predominante: ${JSON.stringify(this.state.out.types[0].type.name)} \n`,
               [
               //  {text: 'Preguntame despues', onPress: () => console.log('Ask me later pressed')},
               //  {text: 'Cancelar', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                 {text: 'Entendido', onPress: () => console.log(`Se abrio el item ${this.state.out.name}`)},
               ]
             ) } 
             else {
              Alert.alert(
                `Advertencia`,
                `Error de conexión: Vuelva e intente nuevamente.\nAsegurarse de tener conexión a Internet.`,
                [
                  {text: 'Entendido', onPress: () => console.log(`Error de conexión.`)},
                ]
              )
             }

        });
    console.log(()=> this.state.out.id);
  }  

  componentDidMount() {

      let text= this.props.route.params?
                      (this.props.route.params.text != null? this.props.route.params.text:""):"";

      let url = 'https://api.thecatapi.com/v1/images/search?limit=12&page=1';//https://mindicador.cl/api' ;//`http://country.io/names.json`;//${this.props.id}
      if (text!="")
      url=`https://pokeapi.co/api/v2/pokemon?limit=${text}`;
  
      fetch(url)
          .then(response => response.json())
          .then(result => {this.setState({ res : result } );console.log('count:',result);});
      
      if(!this.state.res){
        this.setState({ res : [
          {name: 'Devin'},
          {name: 'Dan'},
        ] })
      }

      console.log(this.state.initialPosition)
  }
  
  componentWillUnmount() {
      console.log(this.state.initialPosition)
  }

  App() {//<Image require={this.state.out?.sprites.front_default}/>,backgroundColor:"#0e474f"
    
  return (
    <SafeAreaView style={styles.container2}>
    <ImageBackground style={{ flex: 1, 
          height: '100%',
          width: '100%'}}
          resizeMode="cover"
          source={require('./../assets/gif/fondo-ani.gif')}>
    <View style={{flex: 1, height: "100%", width: "100%",padding:20 }}>
    <Text style={{color:'#fff',fontSize:30}}>Gatitos Aleatorios</Text>
    
    <View style={{visible:this.state.load}}>
    {this.loadCom(this.state.load)}
    </View>
    
    <FlatList
        data={this.state.res}
        keyExtractor={item => item.id.toString()}//({item}) =>item.codigo}  
        ItemSeparatorComponent={this.renderSeparator}
        showsVerticalScrollIndicator={true}
        renderItem={(data) =>
          <TouchableOpacity style={styles.container}
          //onPress={this.getListViewItem.bind(this, item)}
          >
              <Image style={styles.image} source={{ uri: data.item.url }} />
          </TouchableOpacity>
        }/>

  </View>
  </ImageBackground></SafeAreaView>
  );
}

    render(){
      return (this.App())
    };

}

const styles = StyleSheet.create({
  container: {
    height: 300,
    margin: 10,
    backgroundColor: '#FFF',
    borderRadius: 6,
  },
  container2: {
    flex: 1,
    marginTop: 120,
  },
  image: {
    height: '100%',
    borderRadius: 4,
  },
});

export default ListaWS;