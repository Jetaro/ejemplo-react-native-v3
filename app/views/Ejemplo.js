import React, { Component, useState } from 'react'; 
//import { useCallback, useEffect } from 'react';
import {
  Modal,
  Text,
  View,
  Button,
  StyleSheet,
  AppState,
} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import OneSignal from 'react-native-onesignal';

//OneSignal Init Code
OneSignal.setLogLevel(6, 0);
OneSignal.setAppId("a51b9902-e163-455c-abcb-df06a8be4eba");
//END OneSignal Init Code

//Prompt for push on iOS
OneSignal.promptForPushNotificationsWithUserResponse(response => {
  console.log("Prompt response:", response);
});

//Method for handling notifications received while app in foreground
OneSignal.setNotificationWillShowInForegroundHandler(notificationReceivedEvent => {
  console.log("OneSignal: notification will show in foreground:", notificationReceivedEvent);
  let notification = notificationReceivedEvent.getNotification();
  console.log("notification: ", notification);
  const data = notification.additionalData
  console.log("additionalData: ", data);
  // Complete with null means don't show a notification.
  notificationReceivedEvent.complete(notification);
});

//Method for handling notifications opened
OneSignal.setNotificationOpenedHandler(notification => {
  console.log("OneSignal: notification opened:", notification);
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#0e474f'
  },
  mainContainer: {
    marginTop: 22,
    backgroundColor: '#0e474f'
  },
  modalContainer: {
    marginTop: 22,
    backgroundColor: '#0e474f'
  },
});

class Ejemplo extends Component {
  constructor() {
    super();
    this.state = {
      visibility: false,
      cameraPermissionStatus:'not-determined',
      microphonePermissionStatus:'not-determined'
    };

  }

  async componentDidMount() {
    
    /*if (this.state.microphonePermissionStatus == 'not-determined'){
    console.log('Requesting microphone permission...');
    const permission1 = await Camera.requestMicrophonePermission();
    console.log(`Microphone permission status: ${permission1}`);
    
    if (permission1 === 'denied') await Linking.openSettings();
    //setMicrophonePermissionStatus(permission1);
    this.setState({
      visibility: false,
      microphonePermissionStatus:permission1
    });
    }

    if (this.state.cameraPermissionStatus == 'not-determined'){
    console.log('Requesting camera permission...');
    const permission2 = await Camera.requestCameraPermission();
    console.log(`Camera permission status: ${permission2}`);

    if (permission2 === 'denied') await Linking.openSettings();
    //setCameraPermissionStatus(permission2);
    this.setState({
      visibility: false,
      cameraPermissionStatus:permission2
    });
   }*/
  }

  setModalVisibility(visible) {
    this.setState({
      visibility: visible,
    });
  }

  deshacerNavigate() {
    try{
      //this.props.navigation.navigate({name:'Camera'});
    }
    catch(e){
      console.log(e);
      alert ("Se fue a la pulpi")};
  }

  render() {
    return (
      <View style={{backgroundColor: '#0e474f', flex:1}}>
        <View style={{ flex:1}}></View>
        <View style={{ flex:2,padding:20}}>
        <Text style={{fontSize:35,color:'#fff'}}>Ingreso de Actividades</Text>
        </View>
        <View style={{ flex:1}}></View>
        <View style={{ flex:3, padding:20}}>
        <Button
          color="#000"
          onPress={() => this.setModalVisibility(true)}
          title="Ingresar una actividad"
        />
        <View style={{padding:20}}></View>
        <Button color="#000"
        onPress={() => this.deshacerNavigate()} title="Deshacer actividad" />
      </View>
      <View style={{ flex: 1}}> 
        <Modal
          animationType={'fade'}
          transparent={true}
          visible={this.state.visibility}
        >
          <View style={styles.modalContainer}>
            <View style={{padding:20}}>
              <Text style={{fontSize:25,color:'#fff'}}>Ingresar actividad</Text>
              <TextInput placeholder='Descripción' placeholderTextColor={'gray'}></TextInput>
              <TextInput placeholder='% avance' placeholderTextColor={'gray'}></TextInput>
              <TextInput placeholder='HH invertidas' placeholderTextColor={'gray'}></TextInput>
              <Button
                color="#000"
                onPress={() => {this.setModalVisibility(!this.state.visibility)}}
                title="Enviar registro"
              />
              <Button
                color="#000"
                onPress={() => {this.setModalVisibility(!this.state.visibility)}}
                title="Cerrar registro"
              />
            </View>
          </View>
        </Modal>
        </View>
      </View>
    );//this.state.cameraPermissionStatus? :alert("No hay permisos.") 
  }
}

export default Ejemplo;