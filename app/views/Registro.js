import React,{Component} from 'react'
import { StyleSheet, View, Text, Button, Alert, 
  ImageBackground,SafeAreaView, Keyboard, Dimensions } from 'react-native'
import { TextInput } from 'react-native-gesture-handler';
import { openDatabase } from 'react-native-sqlite-storage';
//import BcryptReactNative from 'bcrypt-react-native';

const { width, height } = Dimensions.get('window');
const wd = 320;
const hg = 480;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop:20,
    justifyContent: 'center',
    alignItems:'center',
  },
  container3: {
    flex: 2,
    justifyContent:'center',
    alignItems:'center',
  },
  container4: {
    flex: 1,
    justifyContent:'center',
    alignItems:'center',
  },
  titulo:{
    fontSize: width>wd? 20:20,
    padding: width>wd? 20:30,
    color:'#fff999',
  },
  textInput:{
    width:'70%',
    height:'20%',
    margin:10
  },
  container2: {
    //height: 100,
    //width: 300,
    //position: 'absolute',
    margin: width>=wd? 30:20,
    backgroundColor: 'rgba(30,10,70,0.45)',
    zIndex: 10,
    borderRadius:30,
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
})

class Registro extends Component{
  constructor(props){
    super(props)
  
    this.state = {
      userName:'',
      userAddress:'Nada',
      userContact:'Nada',
      userPass:'',
      vamos:false,
      keyboardStatus: false,
    };
  
  }

  async hasheame(v){ 
    let hash = v;
    /*try {
      let salt = await BcryptReactNative.getSalt(12);
      hash = await BcryptReactNative.hash(salt, v);
      console.log({ hash });
    } catch (e) {
      console.log({ e });
      return v;
    }*/
    return hash;
  }

  registrar(){
    let db = openDatabase({ name: 'UserDatabase.db' });
    let vamos=false;
    let pam =this.state.userName;
    let pum = this.state.userContact;
    let pom = this.state.userPass;

    if (pam.trim().length > 0 && pom.trim().length > 0) {
    let it = this;
    let hash = this.hasheame(this.state.userPass).then(v=>{pom=v}).then(
      setTimeout(() => {
        console.log("1 sec.")
        console.log('pom:', pom)

        db.transaction(function (tx) {
          tx.executeSql(
            'INSERT INTO table_user (user_name, user_pass, user_contact) VALUES (?,?,?)',
            [pam,'a','a'],
            (tx, results) => {
              console.log('Results', results.rowsAffected);
              if (results.rowsAffected > 0) {
                it.props.navigation.navigate('Inicio de sesión');
                Alert.alert(
                  'Mensaje',
                  `Registrado!! `,//\nPass encriptada: ${pom}
                  [{
                      text: 'Ok',
                      onPress: () => { vamos=true; },
                    },],
                  { cancelable: false }
                );} else alert('Fallido!');
            }, 
            (tx, error) => {
              console.log('Error:', error.code);}
          );
          },null,this.update);

      }, 1000));
      
    } else{ 
      Alert.alert('Mensaje', 'Hay campos vacios sin completar.');
      return;}
    };
  
    setPost1(v){
      this.setState({
        userName:v
      })
    }

    setPost2(v){

      this.setState({
        userPass:v
      })
    }
  
    componentDidMount(){ 

    this.keyboardDidShowSubscription = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        this.setState({ keyboardStatus: true });
        //console.log("keyb act")
      },
    );
    this.keyboardDidHideSubscription = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        this.setState({ keyboardStatus: false });
        //console.log("keyb desact")
      },
    );
}


componentWillUnmount = () => {
//<LoadingComponent visible={false} />
this.keyboardDidShowSubscription.remove();
this.keyboardDidHideSubscription.remove();
};

    escanear(){
     // this.props.navigation.navigate("Cargador");
    }

  render(){  
    const flag= this.props.route.params.new!=null? this.props.route.params.new : false;
    return (
      <SafeAreaView style={styles.container}>
      <ImageBackground style={{ flex: 1, 
          height: '100%',
          width: '100%'}}
          resizeMode="cover"
          source={require('./../assets/gif/fondo-ani.gif')}>
        <View style={[styles.container3,{flex: this.state.keyboardStatus? 1:2}]}>
          <Text style={[styles.titulo,this.state.keyboardStatus?{padding:0,paddingLeft:10}:{}]}>
            {flag? 'Registrandote en sistema': 'Registro de usuario'}</Text>
        </View>
        <View style={[styles.container2,{flex: this.state.keyboardStatus? 6:3}]}>
        <TextInput 
          style ={styles.textInput}
          Name="campo1"
          value={this.state.userName}
          onChangeText={(v)=>this.setPost1(v)}
          placeholder="Ingresar nombre-clave"
          placeholderTextColor={"gray"}/>

          <TextInput 
          style ={styles.textInput}
          Name="campo2"
          value={this.state.userPass}
          onChangeText={(v)=>this.setPost2(v)}
          placeholder="Ingresar password"
          secureTextEntry={true}
          placeholderTextColor={"gray"}/>

        <Button
          title="Registrar usuario"
          onPress={() => {
          this.registrar()  
          }}
        />
        </View>
        <View style={[styles.container3,{flex: this.state.keyboardStatus? 0.5:2}]}></View>
      </ImageBackground></SafeAreaView>);
  }
}

export default Registro;