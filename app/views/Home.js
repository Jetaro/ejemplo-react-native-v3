import * as React from 'react';
import {Component,useState} from 'react';
import { View, Text, Button, Alert, ImageBackground, 
  SafeAreaView, StyleSheet, Image, Dimensions} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { openDatabase } from 'react-native-sqlite-storage';
import LoadingComponent from '../components/LoadingComponent';
//import DatePicker from 'react-native-date-picker';
const { width, height } = Dimensions.get('window');
const wd = 320;
const hg = 480;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
})


export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textPa:"",
      estado: false,
      fecha: new Date(), 
    };
  }

  setFecha(date){
    this.setState({
      fecha: date,
    })
  }

  handleSubmit(e) {
    //e.preventDefault();
    console.log('You clicked submit.');
    this.props.navigation.navigate('Probando');
  }

  registro(){
    
    let db = openDatabase({ name: 'UserDatabase.db' });
    let usu = this.props.route.params.text1;
    let tala= 'No estas registrado';

    db.transaction(function (tx) {
      tx.executeSql(
        "SELECT * FROM table_user WHERE user_name = ?",
        [usu],
        (tx, results) => {
          //console.log('Results', results.rows.length);
          if (results.rows.length > 0) {
            Alert.alert(
              'Mensaje',
              'Este usuario esta Registrado!!',
              [{
                  text: 'Ok',
                  onPress: () => {() => this.props.navigation.navigate('Bienvenido');},
                },],
              { cancelable: false }
            );} else alert('Fallido!');
        }, 
        (tx, error) => {
          console.log('Error:', error.code);}
      );
      });

      return tala;
  }

  sesion(v){
    console.log('prn');
    this.setState({
      estado: v
    });
  }

  salir(){}

  componentDidMount() {
    let db = openDatabase({ name: 'UserDatabase.db' });
    let usu = this.props.route.params.text1;
    let it = this;

    db.transaction(function (tx) {
      tx.executeSql(
        "SELECT * FROM table_user WHERE user_name = ?",
        [usu],
        (tx, results) => {
          console.log('Results:', results.rows.length);
          let temp= it;
          if (results.rows.length > 0) {
            it.sesion(true)     
            } else{
              it.props.navigation.navigate('Inicio de sesión');
              Alert.alert(
                'Mensaje',
                'No esta registrado!! Se volvera al inicio de sesion',
                [{
                    text: 'Entendido',
                    onPress: () => {() => salir()},
                  },],
                { cancelable: false }
              );
            }
        }, 
        (tx, error) => {
          console.log('Error:', error.code);}
      );
      });

  }

  componentWillUnmount(){
   /* this.setState({
      load:true
    });

    //<LoadingComponent visible={true}></LoadingComponent>

    setTimeout(() => {
      this.setState({
        load:false
      });
    }, 2000);*/
  }

  handleSubmit2(e) {
    //e.preventDefault();
    console.log('You clicked submit 2.');
    if(this.state.textPa)
    this.props.navigation.navigate({name:'Lista',params:{text: this.state.textPa}});
    else
    this.props.navigation.navigate('Lista');
  }

  render(){
    const img = (this.props.route.params.path!= null)? this.props.route.params.path : '';
    const pam = (this.props.route.params.new!= null)? this.props.route.params.new : false;
    console.log(this.props.route.params.new,pam)
    return (
      <SafeAreaView style={styles.container}>
      <ImageBackground style={{ flex: 1,
            height: '100%',
            width: '100%'}}
            resizeMode="cover"
            source={require('./../assets/gif/fondo-ani.gif')}>
      <View //, backgroundColor: '#0e474f'
      style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <View style={(!pam && height<hg)?{flex:1}:(!pam && height>=hg)?{flex:3}:{flex:1.5}}></View>
        { !pam ?
        (<View style={[{padding:15,justifyContent:'center',alignItems:'center'},height<hg?{flex:3}:{flex:5}]}>
          <Text style={[{ color:"#fff", textAlign:'left' },height<hg?{fontSize:20}:{fontSize:25}]} textAlign='left'>
            Bienvenido otra vez!!!</Text>
          { this.props.route.params.path?
             (<><Text style={[{ color:"#fff", textAlign:'left' },height<hg?{fontSize:20}:{fontSize:25}]} textAlign='left'>
             Aqui tienes la foto que sacaste.</Text><Image
              source={{uri:img}}
              resizeMode='contain'
              style={[{  backgroundColor: 'rgba(30,10,70,0.45)',},height<hg?{height:200,width:200,marginTop:10}:{height:300,width:300,marginTop:20}]}
            /></>):(<></>)
            }
          </View>) 
          :
        (<View style={[{padding:15},height<hg?{flex:3}:{flex:3}]}>
        <Text style={[{ color:"#fff", textAlign:'left' },height<hg?{fontSize:20}:{fontSize:25}]} textAlign='left'>
          {this.state.estado?
                      (this.props.route.params.text1 != null? "Bienvenido a Genesys Ltda.":"Error 404"):"Error 404"} </Text>
        <Text style={[{ color:"#fff" },height<hg?{fontSize:20}:{fontSize:25}]}>{this.state.estado?
                      (this.props.route.params.text1 != null? this.props.route.params.text1:"Credenciales erroneas.Cod 2."):"Credenciales erroneas.Cod 1."}</Text>
        </View>)}
        <View style={{ flex: 0.1}}></View>
        <View style={{ flex: 2, padding:20}}><Text style={{ color:"#fff", textAlign:'left' }}>Por favor, vea una de las opciones en inicio.</Text></View>
      </View></ImageBackground></SafeAreaView>
    );
  }//this.state.fecha
}

/*
<View style={{ flex: 1}}>
        <Text style={{ fontSize:24, color:"#fff" }}>{}</Text>
        <Button
              style ={{paddingHorizontal:8,
                paddingVertical: 6,
                borderRadius: 4,
                backgroundColor: "oldlace",
                alignSelf: "flex-start",
                marginHorizontal: "1%",
                marginBottom: 50,
                minWidth: "48%",
                textAlign: "center"}}
              title="Verificar"
              onPress={()=>this.registro()} />
        </View>
<View style={{ flex: 0.5}}><Button
              style ={{paddingHorizontal:8,
                paddingVertical: 6,
                borderRadius: 4,
                backgroundColor: "oldlace",
                alignSelf: "flex-start",
                marginHorizontal: "1%",
                marginBottom: 50,
                minWidth: "48%",
                textAlign: "center"}}
              title="Ingreso de actividades"
              onPress={()=>this.handleSubmit()} />{/*
              <DatePicker 
              fadeToColor={'orange'}
              mode={'date'}
              date={this.state.fecha} 
              onDateChange={(d)=>{this.setFecha(d)}} />*//*}
              </View>
<View style={{ flex: 0.5, flexDirection:'row'}}>
        <TextInput style={{borderColor: 'gray',borderWidth: 1}} textContentType='creditCardNumber'
              onChangeText={(v)=>this.setState({textPa:v})} 
              placeholder='Numero' placeholderTextColor={'gray'}></TextInput>
          <Button
              style ={{paddingHorizontal:8,
                paddingVertical: 6,
                borderRadius: 4,
                backgroundColor: "oldlace",
                alignSelf: "flex-start",
                marginHorizontal: "1%",
                minWidth: "48%",
                textAlign: "center"}}
              title="Listar Primeros Pokemon"
              onPress={()=>this.handleSubmit2()} />
        </View>

*/
/*<View style={{ flex: 0.5, flexDirection:'row'}}>
        <TextInput style={{borderColor: 'gray',borderWidth: 1}} textContentType='creditCardNumber'
              onChangeText={(v)=>this.setState({textPa:v})} 
              placeholder='Numero' placeholderTextColor={'gray'}></TextInput>
          <Button
              style ={{paddingHorizontal:8,
                paddingVertical: 6,
                borderRadius: 4,
                backgroundColor: "oldlace",
                alignSelf: "flex-start",
                marginHorizontal: "1%",
                minWidth: "48%",
                textAlign: "center"}}
              title="Listar Primeros Pokemon"
              onPress={()=>this.handleSubmit2()} />
        </View>

*/